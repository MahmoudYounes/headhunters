<?php
require_once("includes/initialize.php");
$questions=faq::get_all_questions();
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Head Hunters</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
	<style type="text/css">
	div.error
	{		
		color: 		 black; 
		font-weight: bold;
		font-size:   3em;
		text-align:  center;
		margin: 	 2em auto; 
		padding: 	 1em ;
		width:		 940px;
	}
	div.error ul
	{
		margin: 	 0; 
		padding-top: 1em;
	}
	textarea
	{
		width: 50em;
		height: 30em;
	}

	</style>
</head>

<body>

<div id="header">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="text-decoration:none; color:#b8c6ac; font-size:3em;">HEAD HUNTERS</a>
			</div>
			<?php  if (!$session->is_logged_in()) {?>
			<div class="login">
				<a href="sign_up.php" style="text-decoration:none; color:#b8c6ac">Sign Up </a>/ <a href="sign_in.php" style="text-decoration:none; color:#b8c6ac">Sign in</a>
			</div>
			<?php } else {?>
			<div class="login">
				welcome :) <a href="actions/log_out.php" style="text-decoration:underline; color:#b8c6ac">log out</a>
			</div>			
			<?php }?>
			<ul class="navigation">
				<li>
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="about.php">About</a>
				</li>
				<li>
					<a href="request_job.php">Request Job</a>
				</li>
				<li>
					<a href="hire.php">Hire</a>
				</li>
				<li>
					<a href="request_service.php">Request Service</a>
				</li>
				<li>
					<a href="OfferService.php">Market Service</a>
				</li>				
				<li>
					<a href="profile.php">Profile</a>					
				</li>
				<li  class="active">
					<a href="faq.php">FAQ</a>
				</li>
				<li>
					<a href="contact_us.php">Contact Us</a>
				</li>
			</ul>
		</div>
	</div>
<div id="contents">
	<div class="highlight">
		<div class="clearfix">
			<?php if (!empty($message)&&$message!=""&&isset($message)) {?>
			<div class="error">
				<?php echo $message?>
			</div>
			<?php }?>
<div class="frame2">
	<div class="box">
		<img src="images/thumb-up.jpg" alt="Img" height="298" width="924">
	</div>
</div>	
<?php
	if(count($questions)>1)
	{
		foreach ($questions as $question)
		{
			$output=
			"
			<h2>
			".$question->question."?</h2>
			<p>".$question->answer."
			</p>
			";
			echo $output;
		}
	}
	else if (count($questions)==1)
	{
		$output=
			"
			<h2>
			".$questions->question."?</h2>
			<p>".$questions->answer."
			</p>
			";
			echo $output;
	}
	else
		echo "our FAQ will be available soon";
?>

<!-- footer goes here -->