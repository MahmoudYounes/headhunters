<?php
require_once("includes/initialize.php");

if(!$session->is_admin())
	redirect_to("index.php");

if(!isset($_GET['id']))
	redirect_to("index.php");
if($_GET['id']!=$session->get_current_id())
	redirect_to("AdminLogin.php");
$id=$_GET['id'];
$admin=admin::find_record_by_id($id);
$output=admin::build_on_clearence_level($admin->privilege_level);	//build the output functionality html based on the level of clearence of admin
$message=$session->get_message();
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Head Hunters</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
	<style type="text/css">
	div.error
	{		
		color: 		 black; 
		font-weight: bold;
		font-size:   3em;
		text-align:  center;
		margin: 	 2em auto; 
		padding: 	 1em ;
		width:		 940px;
	}
	div.error ul
	{
		margin: 	 0; 
		padding-top: 1em;
	}

	</style>
</head>

<body>
<div id="header" style = "background-color:#000F1F;">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="padding-left:6.2em;text-decoration:none; color:#F5FFFF; font-size:3em;">HEAD HUNTERS</a>
			</div>
		</div>
</div>
<div id="contents">
	<div class="highlight">
		<div class="clearfix">
			<?php if (!empty($message)&&$message!=""&&isset($message)) {?>
			<div class="error">
				<?php echo $message?>
			</div>
			<?php }?>	
			<h3>Start Manging the website</h3>			
			<?php echo $output; ?>
			<hr>
			<a href="edit_admin_info.php?id={$id}">edit your basic info</a>	
			<a href="actions/log_out.php">Logout </a>		
		</div>
	</div>		
</div>
</body>