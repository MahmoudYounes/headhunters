<?php 
require_once("includes/initialize.php");
$home_context=PagesContent::get_content_by_name("home_context")->content;
$home_quote=PagesContent::get_content_by_name("home_quote")->content;
$offset=strrpos($home_quote,"**QuoteBy**")-1;
$quote=substr($home_quote,0,$offset);
$quoteBy=substr($home_quote,$offset+13);
$news=PagesContent::get_content_by_name("home_news")->content;
?>
<!DOCTYPE HTML>
<!-- Website template by freewebsitetemplates.com -->

<html>
<head>
	<meta charset="UTF-8">
	<title>Head Hunters</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
</head>
<body>
<div style = "background-color:#000F1F;">
	<div id="header" style = "background-color:#000F1F;">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="text-decoration:none; color:#F5FFFF; font-size:3em;">HEAD HUNTERS</a>
			</div>
			<?php if (!($session->is_logged_in())) {?>
			<div class="login">
				<a href="sign_up.php" style="text-decoration:none; color:#F5FFFF;">Sign Up </a>/ <a href="sign_in.php" style="text-decoration:none; color:#F5FFFF">Sign in</a>
			</div>
			<?php } else {?>
			<div class="login" style = "color:#F5FFFF background-color:#000F1F">
				welcome :) <a href="actions/log_out.php" style="text-decoration:underline; color:#F5FFFF">log out</a>
			</div>			
			<?php }?>
	
		
			<ul class="navigation">
				<li class="active">
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="about.php">About</a>
				</li>
				<li>
					<a href="request_job.php">Request Job</a>
				</li>
				<li>
					<a href="hire.php">Hire</a>
				</li>
				<li>
					<a href="request_service.php">Request Service</a>
				</li>
				<li>
					<a href="OfferService.php">Market Service</a>
				</li>				
				<li>
					<a href="profile.php">Profile</a>					
				</li>
				<li>
					<a href="faq.php">FAQ</a>
				</li>
				<li>
					<a href="contact_us.php">Contact Us</a>
				</li>
			</ul>
		</div>
	</div>
	</div>
	<div id="contents">		
		<div class="highlight">
			<div class="clearfix">
				<div class="testimonial">
					<h2>Our Motto</h2>
					<p>
						&ldquo;<?php echo $quote ?>&rdquo;
					</p>
					<span>-<?php echo $quoteBy ?>-</span>
				</div>
				<h1>Brief about Head Hunters</h1>
				<p>
					<?php echo $home_context;?>				
				</p>
			</div>
		</div>		
	</div>
	<div id="footer">
		<div class="clearfix">
			<div class="section">
				<h4>Latest News</h4>
				<p>
					<?php echo $news ?>
				</p>
			</div>
			<div class="section contact">
				<h4>Contact Us</h4>				
				<p>
					<span>Phone:</span> (+20) 012 0881 6682
				</p>
				<p>
					<span>Email:</span> HeadHunters@AUC-CU.com
				</p>
			</div>
			<div class="section">
				<h4>SEND US A MESSAGE</h4>
				<p>
					If you're having problems through any part of this website. Contact Us or visit our forum pages.
				</p>
				<a href="#" class="subscribe">Click to send us an email</a>
			</div>
		</div>
		<div id="footnote">
			<div class="clearfix">
				<div class="connect">
					<a href="#" class="facebook"></a><a href="#" class="twitter"></a><a href="#" class="googleplus"></a><a href="#" class="pinterest"></a>
				</div>
				<p>
					© Copyright 2015 @AUC-CU. All Rights Reserved.
				</p>
			</div>
		</div>
	</div>
</body>
</html>
	