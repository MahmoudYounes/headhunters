<?php 
	require_once("includes/config.php");
	require_once("includes/initialize.php");
	require_once("includes/functions.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Search Results</title>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <!--<link rel="stylesheet" type="text/css" href="css/sign_up_style.css" media="all" />-->
	<link rel="stylesheet" href="css/request_service_style.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="css/main_style.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="css/myButton.css" type="text/css" media="all"/>


	<script type="text/javascript" src="js/js_functions.js"></script>
</head>
<body>



 
 
 <!--<div class="container" style = "height: 100%;">-->
			<div id="header">
			<div class="clearfix">
				<div class="logo">
					<a href="index.php" style="text-decoration:none; color:#b8c6ac; font-size:3em;">HEAD HUNTERS</a>
				</div>
				<?php  if (!$session->is_logged_in()) {?>
				<div class="login">
					<a href="sign_up.php" style="text-decoration:none; color:#b8c6ac">Sign Up </a>/ <a href="sign_in.php" style="text-decoration:none; color:#b8c6ac">Sign in</a>
				</div>
				<?php } else {?>
				<div class="login">
					welcome :) <a href="actions/log_out.php" style="text-decoration:underline; color:#b8c6ac">log out</a>
				</div>			
				<?php }?>
				<ul class="navigation">
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="about.php">About</a>
					</li>
					<li>
						<a href="request_job.php">Request Job</a>
					</li>
					<li class="active">
						<a href="hire.php">Hire</a>
					</li>
					<li>
						<a href="request_service.php">Request Service</a>
					</li>
					<li>
						<a href="OfferService.php">Market Service</a>
					</li>				
					<li>
						<a href="profile.php">Profile</a>					
					</li>
					<li>
						<a href="faq.php">FAQ</a>
					</li>
					<li>
						<a href="contact_us.php">Contact Us</a>
					</li>
				</ul>
			</div>
			</div><br></br>
			<!--<div  class="form" style = "height:30px;">-->
			
		<div id = "results" style = "align:center; padding-left:300px;">
			<?php
	$result = search_for_job_tags($_GET['search']);
	if($result === FALSE) { 
    die("no match found"); // TODO: better error handling
}
?> 


<?php
$found=false;
while($row = mysqli_fetch_array($result))
{
	
   ?>
   
	
	<?php
	if ($row['AID']!=$_SESSION['id'])
	{?>
		<div   class="form"  style = "height:60px;  width:500px;" ><font face="verdana" color="#599bb3" size = "4">
		<?php
		$found=true;
		echo $row['username'];
		echo "</br>";
		echo $row['skills'];
		echo "</br></br>";
		?>
		<span><a href="#" class="myButton" >visit profile</a></span>
					  <br/><br/>
		</div><br></br>
		<?php
	}

}
	if (!$found)
		echo "no match found<br/>";	
	?>
	</font>

</div><br></br>			
		

 
 <div id="footer">
 
			<div class="clearfix">
				<div class="section">
					<h4>Latest News</h4>
					<p>
						any brand new news about our website will be posted here. if any upcoming feature is added we shall announce here.
					</p>
				</div>
				<div class="section contact">
					<h4>Contact Us</h4>				
					<p>
						<span>Phone:</span> (+20) 012 0881 6682
					</p>
					<p>
						<span>Email:</span> HeadHunters@AUC-CU.com
					</p>
				</div>
				<div class="section">
					<h4>SEND US A MESSAGE</h4>
					<p>
						If you're having problems through any part of this website. Contact Us or visit our forum pages.
					</p>
					<a href="#" class="subscribe">Click to send us an email</a>
				</div>
			</div>
			<div id="footnote">
				<div class="clearfix">
					<div class="connect">
						<a href="#" class="facebook"></a><a href="#" class="twitter"></a><a href="#" class="googleplus"></a><a href="#" class="pinterest"></a>
					</div>
					<p>
						© Copyright 2015 @AUC-CU. All Rights Reserved.
					</p>
				</div>
			</div>
		</div>
		  
		</body>
</html>
