<?php
require_once("httpful.phar");

$appID = "";
$appKey = "";
 
$url = "http://api.tuxx.co.uk/demo/server/time.php&app_id=" . $appID . "&app_key=" . $appKey;
 
// Make the REST request
$response = \Httpful\Request::get($url)->send();
 
if (!$response->hasErrors()) {
    // Display the server response
    var_dump($response->body);
} else {
    // The connection failed (maybe the application ID or application key is not valid)
    echo "Uh oh. The connection to the Tuxx API failed.";
}
?>