<?php
require_once('includes/initialize.php');
	if (!$session->is_logged_in())
	{
		redirect_to('sign_in.php');
	}
	
	$errors = array();
	$tags_arr=tags::retrieve_all_tags();
	if(isset($_POST["submit"]))
	{
		$max = 300;
		$min = 5;
		$desc = str_replace(' ', '', $_POST["stitle"]);
		
		if(empty($desc))
		{
			$errors['stitle'] = "* The description can't be blank";
		}
		
		else if(strlen($_POST["stitle"]) < $min)
		{
			$errors['stitle'] = "* The description is too short";
		}
			
		else if (strlen($_POST["stitle"]) > $max)
		{
			$error['stitle'] = "* The description is too tall";
		}
			
		else 
		{
			$errors['stitle'] = "";
			$Sdesc = $_POST["stitle"];					
		
			$tags = $_POST["Tags"];		
			$AID = $_SESSION['id'];
			
			$o_service=new OfferService();
			$o_service->sdescription=$Sdesc;
			$o_service->AID=$AID;
			$o_service->tags=$tags;

			if(!($o_service->create()&&$o_service->insert_tags()))
				die("could not insert offered service");
			else redirect_to("index.php");
			
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
<title>Head Hunters/Offer Service</title>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href="css/sign_up_style.css" media="all" />
	 <link rel="stylesheet" type="text/css" href="css/main_style.css" media="all" />
	 <script src = "js/js_functions.js"></script>
</head>
<body style = "background-color: #E7EDF9">
	<div id="header" style = "background-color:#000F1F ">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="text-decoration:none; color:#F5FFFF; font-size:3em;">HEAD HUNTERS</a>
			</div>
				<div class="login">
					welcome :) <a href="actions/log_out.php" style="text-decoration:underline; color:#F5FFFF">log out</a>
				</div>			
			<ul class="navigation">
				<li>
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="about.php">About</a>
				</li>
				<li>
					<a href="request_job.php">Request Job</a>
				</li>
				<li>
					<a href="hire.php">Hire</a>
				</li>
				<li>
					<a href="request_service.php">Request Service</a>
				</li>
				<li >
					<a href="OfferService.php">Market Service</a>
				</li>				
				<li>
					<a href="profile.php">Profile</a>					
				</li>
				<li>
					<a href="faq.php">FAQ</a>
				</li>
				<li>
					<a href="contact_us.php">Contact Us</a>
				</li>
			</ul>
		</div>
	</div>
<div class="container">
			
            
			<header>
				<h1 style = "color: #335C85">MARKET SERVICE</h1>
            </header>       
      <div  class="form" style = "height: 450px;" >
    		<form id="serviceform" action = "<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method = "post"> 
    			
				<p class="contact"><label for="stitle">Service Description</label></p>
    			<input id="stitle" name="stitle" placeholder="Service Description" required="" tabindex="1" type="text">
				<span><small style = "color:red"><?php if(isset($errors['stitle'])) echo($errors['stitle'])?></small></span>
				<br></br>
		
			<div id="greater_selection">
					  <p class = "contact"><label>Search Tags</label></p>					 
					  	<div class="small_selection" id="original_selection">
						  <select class="select-style Tags" name="Tags[]" style = "width: 400px" id="selections">
						  	<?php					  		
						  		foreach ($tags_arr as $a_tag)
						  		{
						  			$output = "<option value=\"{$a_tag->TID}\">{$a_tag->tname}</option>";
						  			echo $output;
						  		}
						  	?>
						  </select><br/><br/>
						 </div>
					  </div>
					  <button name="addtags" type="button" onclick="create_selection_tag()">add another tag</button>
					  <br/><br/>
			
            
			<p class="contact"><label for="phone">Upload Picture</label></p>
			  <input 
			  <input type="file" enctype="multipart/form-data" name="pic" accept="image">
			  <br></br>
         <!-- <input type="submit"> -->
			
			<p class="contact"><label for="phone">Upload Video</label></p>
            <input type="file" enctype="multipart/form-data" name="vid" accept="video/*" >
			<br></br>
			
            <input class="buttom" align = "center" name="submit"  id="submit" tabindex="5" value="Upload my Service" type="submit"> 	 
   </form> 
</div>      
</div>

<div id="footer">
		<div class="clearfix">
			<div class="section">
				<h4>Latest News</h4>
				<p>
					any brand new news about our website will be posted here. if any upcoming feature is added we shall announce here.
				</p>
			</div>
			<div class="section contact">
				<h4>Contact Us</h4>				
				<p>
					<span>Phone:</span> (+20) 012 0881 6682
				</p>
				<p>
					<span>Email:</span> HeadHunters@AUC-CU.com
				</p>
			</div>
			<div class="section">
				<h4>SEND US A MESSAGE</h4>
				<p>
					If you're having problems through any part of this website. Contact Us or visit our forum pages.
				</p>
				<a href="#" class="subscribe">Click to send us an email</a>
			</div>
		</div>
		<div id="footnote">
			<div class="clearfix">
				<div class="connect">
					<a href="#" class="facebook"></a><a href="#" class="twitter"></a><a href="#" class="googleplus"></a><a href="#" class="pinterest"></a>
				</div>
				<p>
					© Copyright 2015 @AUC-CU. All Rights Reserved.
				</p>
			</div>
		</div>
	</div>

</body>
</html>