<?php
require_once("includes/initialize.php");

$admins=admin::retrieve_all_admins();

$message=$session->get_message();
$admin_id=$session->get_current_id();
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Head Hunters</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
	<style type="text/css">
	div.error
	{		
		color: 		 black; 
		font-weight: bold;
		font-size:   3em;
		text-align:  center;
		margin: 	 2em auto; 
		padding: 	 1em ;
		width:		 940px;
	}
	div.error ul
	{
		margin: 	 0; 
		padding-top: 1em;
	}
	.table
	{		
		width:  100%;	
		text-align: center;
	}
	

	</style>
</head>

<body>
<div id="header" style = "background-color:#000F1F;">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="padding-left:6.2em;text-decoration:none; color:#F5FFFF; font-size:3em;">HEAD HUNTERS</a>
			</div>
		</div>
</div>
<div id="contents">
	<div class="highlight">
		<div class="clearfix">
			<?php if (!empty($message)&&$message!=""&&isset($message)) {?>
			<div class="error">
				<?php echo $message?>
			</div>
			<?php }?>
			<h1 style="text-align:center;">Admin List</h1> 	
			<p style="float:left;margin-right:0.5em"><a href="AdminIndex.php?id=<?php echo $admin_id?>">admin index</a></p>			
			<?php if (count($admins)>1) {?>
			<p><a href="actions/add_admin.php"> add an admin</a></p>			
			<table class="table" border="1">
			<th>username</th>
			<th>email</th>
			<th>privilege level</th>
			<th>operate on</th>	
			<?php			
				foreach ($admins as $admin)
				{
					if($admin->privilege_level!=0)	//assumption: cant remove admin level zero 
					{
						echo "<tr>";
						$output=$admin->format_admin_for_output_in_table();
						echo $output;
						echo "</tr>";
					}
				}			
			?>
			</table>
			<?php } else echo "<p style=\"text-align:center\">no admins to reveal.<a href=actions/add_admin.php>add an admin? </a></p>";?>	
		</div>
	</div>
</div>