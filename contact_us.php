<!DOCTYPE HTML>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Contact - Law Firm</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="text-decoration:none; color:#b8c6ac; font-size:3em;">HEAD HUNTERS</a>
			</div>
			<div class="login">
				<a href="#" style="text-decoration:none; color:#b8c6ac">Sign Up </a>/ <a href="#" style="text-decoration:none; color:#b8c6ac">Sign in</a>
			</div>
			<ul class="navigation">
					<li>
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="about.php">About</a>
				</li>
				<li>
					<a href="request_job.php">Request Job</a>
				</li>
				<li>
					<a href="hire.php">Hire</a>
				</li>
				<li>
					<a href="request_service.php">Request Service</a>
				</li>
				<li>
					<a href="market_service.php">Market Service</a>
				</li>				
				<li>
					<a href="profile.php">Profile</a>					
				</li>
				<li>
					<a href="faq.php">FAQ</a>
				</li>
				<li class="active">
					<a href="contact_us.php">Contact Us</a>
				</li>
			</ul>
		</div>
	</div>

				
	<div id="contents">
		<div class="clearfix">
			<div class="sidebar">
				<div>
					<h2>Contact Info</h2>
					<ul class="contact">
						<li>
							<p>
								<span class="home"></span> <em>HEAD HUNTERS<br></em>
							</p>
						</li>
						<li>
							<p>
								Phone: (+20) 012 0881 6682
							</p>
						</li>						
						<li>
							<p>
								Email: HeadHunters@AUC-CU.com
							</p>
						</li>
					</ul>
				</div>
			</div>
			<div class="main">
				<h1>Contact</h1>
				<h2>Send Us a Quick Message</h2>
				<p>
					tell us what bothers you and we shall respond as fast as we can. glad we will hear from you.
				</p>
				<form action="index.html" method="post" class="message">
					<label>First Name</label>
					<input type="text" value="">
					<label>Last Name</label>
					<input type="text" value="">
					<label>Email Address</label>
					<input type="text" value="">
					<label>Message</label>
					<textarea></textarea>
					<input type="submit" value="Send Message">
				</form>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="clearfix">
			<div class="section">
				<h4>Latest News</h4>
				<p>
					any brand new news about our website will be posted here. if any upcoming feature is added we shall announce here.
				</p>
			</div>
			<div class="section contact">
				<h4>Contact Us</h4>				
				<p>
					<span>Phone:</span> (+20) 012 0881 6682
				</p>
				<p>
					<span>Email:</span> HeadHunters@AUC-CU.com
				</p>
			</div>
			<div class="section">
				<h4>SEND US A MESSAGE</h4>
				<p>
					If you're having problems through any part of this website. Contact Us or visit our forum pages.
				</p>
				<a href="#" class="subscribe">Click to send us an email</a>
			</div>
		</div>
		<div id="footnote">
			<div class="clearfix">
				<div class="connect">
					<a href="#" class="facebook"></a><a href="#" class="twitter"></a><a href="#" class="googleplus"></a><a href="#" class="pinterest"></a>
				</div>
				<p>
					© Copyright 2015 @AUC-CU. All Rights Reserved.
				</p>
			</div>
		</div>
	</div>
</body>
</html>