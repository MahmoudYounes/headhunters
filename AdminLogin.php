<?php
require_once("includes/initialize.php");

if($session->is_logged_in())
	redirect_to("index.php");

if (isset($_POST['submit']))
{
	//TODO : 
	//1-validate Data
	//2-process admin login

	// validating data

	//=============================================\\

	//processing login
	$username=$_POST['username'];
	$password=$_POST['password'];

	if($admin=admin::authenticate($username,$password))
	{
		$session->mark_admin_login($admin);
		admin_log_action("LogIn: ".$admin->username." logged in",get_current_date());	
		$session->set_message("Welcome {$username}");		
		redirect_to("AdminIndex.php?id=".$admin->ADID);	
	}
	else
	{
		?>
		<script type="text/javascript">
		alert("username or password are incorrect");
		</script>
		<?php	
	}
	//===============================================\\

}



?>

<!DOCTYPE html>
<html>

<head>

  <meta charset="UTF-8">

  <title>Head Hunters/sign in</title>
  <link rel="stylesheet" type="text/css" href="css/sign_in_style.css" media="all" />
  <link rel="stylesheet" type="text/css" href="css/sign_up_style.css" media="all" />
   <style>
@import url(http://fonts.googleapis.com/css?family=Exo:100,200,400);
@import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);

body{
	margin: 0;
	padding: 0;
	background-image: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH9H7aj5xnB-I-6QRfp7HTAoAiMDOWW_MW8-4VykIJUAfiBwGj);
	color: #0000;
	font-family: "Times New Roman", Times, serif;
	font-size: 20px;
}

.body{
	position: absolute;
	top: -20px;
	left: -20px;
	right: -40px;
	bottom: -40px;
	width: auto;
	height: auto;
	background-image: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH9H7aj5xnB-I-6QRfp7HTAoAiMDOWW_MW8-4VykIJUAfiBwGj);
	background-size: cover;
	-webkit-filter: blur(5px);
	z-index: 0;
}

.grad{
	position: absolute;
	top: -20px;
	left: -20px;
	right: -40px;
	bottom: -40px;
	width: auto;
	height: auto;
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
	z-index: 1;
	opacity: 0.7;
}

.header{
	position: absolute;
	top: calc(50% - 35px);
	left: calc(50% - 255px);
	z-index: 2;
}

.header div{
	float: left;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 35px;
	font-weight: bold;
}

.header div span{
	color: #fff !important;
}

.login{
	position: absolute;
	top: calc(50% - 75px);
	left: calc(50% - 50px);
	height: 150px;
	width: 350px;
	padding: 10px;
	z-index: 2;
	margin-right: 10px;

.login input[type=text]{
	width: 250px;
	height: 30px;
	background: #fff;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: black;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
}

.login input[type=password]{
	width: 250px;
	height: 30px;
	background: #fff;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: black;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
	margin-top: 10px;
}

.login input[type=submit]{
	width: 260px;
	height: 35px;
	background: #808080;
	border: 1px solid #fff;
	cursor: pointer;
	border-radius: 2px;
	color:white;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 6px;
	margin-top: 10px;
}

.login input[type=submit]:hover{
	opacity: 0.8;
}

.login input[type=submit]:active{
	opacity: 0.6;
}

.login input[type=text]:focus{
	outline: none;
	border: 1px solid rgba(255,255,255,0.9);
}

.login input[type=password]:focus{
	outline: none;
	border: 1px solid rgba(255,255,255,0.9);
}

.login input[type=submit]:focus{
	outline: none;
}

::-webkit-input-placeholder{
   color: rgba(255,255,255,0.6);
}

::-moz-input-placeholder{
   color: rgba(255,255,255,0.6);
}

::-webkit-input-placeholder {
   color: black;
}

:-moz-placeholder { /* Firefox 18- */
   color: black;  
}

::-moz-placeholder {  /* Firefox 19+ */
   color: black;  
}

:-ms-input-placeholder {  
   color: black;  
}
</style>
	<script src="js/prefixfree.min.js"></script>
</head>

<body>	 
	<div class="body"></div>
	<div class="grad"></div>
		<div class="header">				
			<div>Head<span>Hunters</span><br/>Admin Login</div>
		</div>	
		<div class="freshdesignweb-top">
                <a href="index.php" target="_blank" style = "color: #fff">Home</a>
                <div class="clr"></div>
            </div><!--/ freshdesignweb top bar --> 	
			<form method="POST" action="AdminLogin.php" class="login">
				<input type="text" name="username" placeholder = "username" style = "margin-left: 10px;">
				<input type="password" placeholder="password" name="password"  style = "margin-left: 10px;">
				<input type="submit" name="submit" value="Sign in" style = "margin-left: 10px;">
			</form>

  <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
  

</body>
</html>