<?php
require_once("../includes/initialize.php");

if(!$session->is_admin())
	redirect_to("index.php");
if(isset($_POST['submit']))
{
	//===========================================
	//TODO : validate data:
		//-max length 10 chars for tname not 



	//===========================================
	$tag=new tags();
	$tag->tname=$_POST['tname'];
	$tag->tdescription=$_POST['tdescription'];
	$tag->save();
	$session->set_message("tag added successfully");
}
$message=$session->get_message();
?>
<?php require_once("actions_html_css_header.php");?>
<p><a href="../edit_tags.php">go back to tags list</a></p>
<form class="tag-form" method="post" action="add_tag.php">
	<table>
		<tr>
			<td>tag name</td>
		</tr>				
		<tr>
			<td><input type="text" name="tname"></td>
		</tr>
		<tr>
			<td>tag description</td>
		</tr>
		<tr>
			<td><textarea row="5" col="60" name="tdescription"> </textarea></td>
		</tr>
		<tr>
			<td><input type="submit" name="submit" value="submit"></td>
		</tr>
	</table>
</form>
<?php require_once("actions_html_css_footer.php");?>