<html>
<head>
	<meta charset="UTF-8">
	<title>Head Hunters</title>
	<link rel="stylesheet" href="../css/main_style.css" type="text/css">
	<style type="text/css">
	div.error
	{		
		color: 		 black; 
		font-weight: bold;
		font-size:   3em;
		text-align:  center;
		margin: 	 2em auto; 
		padding: 	 1em ;
		width:		 940px;
	}
	div.error ul
	{
		margin: 	 0; 
		padding-top: 1em;
	}
	.table
	{		
		width:  100%;	
		text-align: center;
	}
	.tag-form
	{
		width: 	50%;
		margin: 0 auto;		

	}
	</style>
</head>

<body>
<div id="header" style = "background-color:#000F1F;">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="padding-left:6.2em;text-decoration:none; color:#F5FFFF; font-size:3em;">HEAD HUNTERS</a>
			</div>
		</div>
</div>
<div id="contents">
	<div class="highlight">
		<div class="clearfix">
			<?php if (!empty($message)&&$message!=""&&isset($message)) {?>
			<div class="error">
				<?php echo $message?>
			</div>
			<?php }?>