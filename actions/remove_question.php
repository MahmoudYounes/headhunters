<?php
require_once("../includes/initialize.php");

if(!$session->is_admin())
	redirect_to("../index.php");

if(!isset($_GET['id']))
	redirect_to("../AdminIndex.php");
$question=new faq();
$question->QAID=$_GET['id'];
$question->delete();

$session->set_message("question deleted successfully");
redirect_to("../edit_faq.php");
?>