<?php
require_once("../includes/initialize.php");

if(!$session->is_admin())
	redirect_to("../index.php");

if(!isset($_GET['id']))
	redirect_to("../AdminIndex.php");
$admin=new admin();
$admin->ADID=$_GET['id'];
$admin->delete();

$session->set_message("admin deleted successfully");
redirect_to("../manage_admins.php");
?>