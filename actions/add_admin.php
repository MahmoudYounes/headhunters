<?php
require_once("../includes/initialize.php");
if(!$session->is_admin())
	redirect_to("index.php");
if(isset($_POST['submit']))
{
	//===========================================
	//TODO : validate data:		



	//===========================================
	$new_admin=new admin();
	$new_admin->username=$_POST['adusername'];
	$password=$_POST['adpassword'];
	$encrypted_password= password_encrypt($password);
	$new_admin->password=$encrypted_password;
	$new_admin->email=$_POST['ademail'];
	$new_admin->privilege_level=$_POST['adlevel'];
	$new_admin->save();
	$session->set_message("admin inserted successfully");

}
$message=$session->get_message();

?>
<?php require_once("actions_html_css_header.php");?>
<p><a href="../manage_admins.php">go back to admins list</a></p>
<form class="admin-form" method="post" action="add_admin.php">
	<table>
		<tr>
			<td>admin username</td>
		</tr>				
		<tr>
			<td><input type="text" name="adusername"></td>
		</tr>
		<tr>
			<td>admin password</td>
		</tr>				
		<tr>
			<td><input type="password" name="adpassword"></td>
		</tr>
		<tr>
			<td>re-enter password</td>
		</tr>				
		<tr>
			<td><input type="password" name="re-adpassword"></td>
		</tr>
		<tr>
			<td>email</td>
		</tr>
		<tr>
			<td><input type="email" name="ademail"></td>
		</tr>
		<tr>
			<td>privilege level</td>
		</tr>
		<tr>
			<td>
			<select name="adlevel">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
			</td>
		</tr>
		<tr>
			<td><input type="submit" name="submit" value="submit"></td>
		</tr>
	</table>
</form>
<?php require_once("actions_html_css_footer.php");?>