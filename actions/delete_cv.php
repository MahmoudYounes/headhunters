<?php
require_once("../includes/initialize.php");

if(!$session->is_logged_in())		
	redirect_to("../index.php");
if(!isset($_GET['id']))	
	redirect_to("../profile.php");
$cv=new cvs();
$cv->CID=$_GET['id'];
$cv->delete();

$session->set_message("CV deleted Successfuly");

redirect_to("../profile.php");
?>	