<?php
require_once("../includes/initialize.php");

if(!$session->is_admin())
	redirect_to("../index.php");

if(!isset($_GET['id']))
	redirect_to("../AdminIndex.php");
$tag=new tags();
$tag->TID=$_GET['id'];
$tag->delete();

$session->set_message("tag deleted successfully");
redirect_to("../edit_tags.php");
?>