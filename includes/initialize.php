<?php

defined('DS')? null: define('DS',DIRECTORY_SEPARATOR);


/* change the absolute file path here and it will be changed in all files */
defined('SITE_ROOT')? null: define('SITE_ROOT','C:'.DS.'wamp'.DS.'www'.DS.'my_hh');

/* directory of includes folder*/ 
defined('INC')? null:define("INC",SITE_ROOT.DS."includes");

/* directory of classes folder*/
defined('CLS')? null:define("CLS",SITE_ROOT.DS."classes");

/* directory for log files*/
defined('LOG')? null:define("LOG",SITE_ROOT.DS."logs");

/*endline character 3ashan ana zh2t*/
defined('ENDL')? null:define("ENDL","<br/>");


/* Including required files for project */

/* database configurations */
require_once(INC.DS."config.php");

/* basic functions */
require_once(INC.DS."functions.php");


/* including classes files for project */

require_once(CLS.DS."database_class.php");
require_once(CLS.DS."session_class.php");
require_once(CLS.DS."table_class.php");
require_once(CLS.DS."account_class.php");
require_once(CLS.DS."tags_class.php");
require_once(CLS.DS."admin_class.php");
require_once(CLS.DS."RequestService_class.php");
require_once(CLS.DS."PagesContent_class.php");
require_once(CLS.DS."faq_class.php");
require_once(CLS.DS."cv_class.php");
require_once(CLS.DS."CVSection_class.php");








?>