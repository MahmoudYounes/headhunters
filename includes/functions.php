<?php
	
	function redirect_to($new_location)
	{
		header("Location: " . $new_location);
		exit;
	}
	
	

	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	function fieldname_as_text($fieldname)
	{
		$fieldname = str_replace("_", " ", $fieldname);
		$fieldname = ucfirst($fieldname);
		return $fieldname;
	}


	/* 
		-this function populates a log file with messages like user log_ins and messages
		-Input: action and message
		-Ouput: true or false 
	*/
	function log_action($action,$message="")
	{		
		$file_path=LOG."/logfile.txt";
		$f_handle=null;
		$message.=" ".$action."\r\n";
		if (file_exists($file_path))
		{
			//open file;
			if (!($f_handle=fopen($file_path,'a')))
				die("failed to open file: ".$file_path);
		}
		else
		{
			//create file and open in write mode
			$f_handle=fopen($file_path,'w');
		}
		fwrite($f_handle, $message);
	}

	function admin_log_action($action,$message="")
	{
		$file_path=LOG."/Adminlogfile.txt";
		$f_handle=null;
		$message.=" ".$action."\r\n";
		if (file_exists($file_path))
		{
			//open file;
			if (!($f_handle=fopen($file_path,'a')))
				die("failed to open file: ".$file_path);
		}
		else
		{
			//create file and open in write mode
			$f_handle=fopen($file_path,'w');
			fwrite($f_handle, $message);
		}		
	}


	/*
		-this function returns current date & time
	*/
	function get_current_date()
	{
		$output="";		
		$date=strftime("%Y/%d/%m %H:%M:%S",time());
		$output.=$date;
		$output.=" | ";
		return $output;
	} 



	/*
		-this function auto activates when using a class or functions that are not included in a page requiring it (might search in multiple paths)
		-Input: auto => class_name or function name
		-Output: navigates to the directory given and includes the path.
	*/
	function __autoload($class_name)
	{
		$class_name=strtolower($class_name);
		$path=CLS.DS."{$class_name}_class.php";
		if(file_exists($path))
			require_once($path);
		else
			die("the file {$class_name}.php was not found in the path {$path}");
	}
	
	//not used function to generate output in a cleaner way	
	function form_errors($errors=array()) {
		$output = "";
		if (!empty($errors)) {
		  $output .= "<div class=\"error\">";
		  $output .= "Please fix the following errors:";
		  $output .= "<ul>";
		  foreach ($errors as $key => $error) {
		    $output .= "<li>";
		    $output .= htmlentities($error) . "</li>";
		  }
		  $output .= "</ul>";
		  $output .= "</div>";
		}
		return $output;
	}	

	function password_encrypt($password) 
	{
		$hash_format = "$2y$10$";   // Tells PHP to use Blowfish with a "cost" of 10
		$salt_length = 22; 					// Blowfish salts should be 22-characters or more
		$salt = generate_salt($salt_length);
		$format_and_salt = $hash_format . $salt;
		$hash = crypt($password, $format_and_salt);
		return $hash;
	}

	function generate_salt($length) 
	{
		// Not 100% unique, not 100% random, but good enough for a salt
		// MD5 returns 32 characters
		$unique_random_string = md5(uniqid(mt_rand(), true));
		
		// Valid characters for a salt are [a-zA-Z0-9./]
		$base64_string = base64_encode($unique_random_string);
		
		// But not '+' which is valid in base64 encoding
		$modified_base64_string = str_replace('+', '.', $base64_string);

		// Truncate string to the correct length
		$salt = substr($modified_base64_string, 0, $length);

		return $salt;
	}
		
	function password_check($password, $existing_hash) 
	{
		// existing hash contains format and salt at start
		$hash = crypt($password, $existing_hash);
		if ($hash === $existing_hash) 
		{
			return true;
		}
		else 
		{
			return false;
		}
	}

	/*
	function insert_offered_service($tags,$AID,$Sdesc)
	{
		global $connection;
			//$sql = "SELECT * FROM service_offers WHERE (sdescription = '{$Sdesc}' AND AID = '{$AID}');";
			//$result=mysqli_query($connection,$sql);
			/*if(mysqli_num_rows($result) > 0)
			{
				echo ("You already submitted that service");
			}
				
			
			$sql1 = "INSERT INTO service_offers (sdescription,AID) VALUES ('{$Sdesc}',{$AID});";
			
			$result = mysqli_query($connection,$sql1);
			if(!$result)
			{
				die("Submission Failed.1");
			}
			else
			{
				$sid = mysqli_insert_id($connection);
				for($i=0; $i< count($tags); $i++)
				{
					$sql1 = "INSERT INTO service_tags (SID,TID) VALUES ({$sid},{$tags[$i]});";
					$result = mysqli_query($connection,$sql1);
					if(!$result)
					{
						die("Submission Failed.2");
					}
				}
				redirect_to("index.php");
			}
	}
	*/
	
	/* search function */
	function search_for_tags($search_item)
	{	
		global $db;

		$query  = "SELECT * ";
		$query .= "FROM tags T,account A,service_tags ST,service_offers SO ";
		$query .= "WHERE T.tname= '{$search_item}' ";
		$query .= "AND T.TID = ST.TID ";
		$query .= "AND ST.SID = SO.SID ";
		$query .= "AND SO.AID = A.AID ";
		
		
		
		$result = $db->query($query);
		
		
		
		return $result;
	}
	
	function search_for_hire($search_item)
	{	
		global $db;

		$query  = "SELECT * ";
		$query .= "FROM tags T,account A,service_tags ST,service_offers SO ";
		$query .= "WHERE T.tname= '{$search_item}' ";
		$query .= "AND T.TID = ST.TID ";
		$query .= "AND ST.SID = SO.SID ";
		$query .= "AND SO.AID = A.AID ";
		
		
		
		$result = $db->query($query);
		
		
		
		return $result;
	}

	function search_for_job_tags($search_tag)
	{
		global $db;

		$query  = "SELECT * ";
		$query .= "FROM tags T,account A,cv_tags cT,cvs c ";
		$query .= "WHERE T.tname= '{$search_tag}' ";
		$query .= "AND T.TID = cT.TID ";
		$query .= "AND cT.CID = c.CID ";
		$query .= "AND c.AID = A.AID ";
		$query .= "AND c.Market = '1'";
				
		$result = $db->query($query);
		
		return $result;
	}
?>
