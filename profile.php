<?php
require_once('includes/initialize.php');
if(!$session->is_logged_in())
{
	redirect_to("sign_in.php");
}
$cond=false;      //condition true: if an outsider is viewing the page
if (isset($_GET['id']))
{
	//another one viewing the account so he can hire him .. page output must be put here
	$cond=true;
	$visited_id=$_GET['id'];
	if($visited_id==$session->get_current_id()) //he is trying to directly access his profile by GET request 
		redirect_to("profile.php");
}
else
{
	if(isset($_POST['submit']))
	{
		if(!empty($_FILES["image"]["name"]))
		{
			if(isset($_FILES["image"]["tmp_name"])&&isset($_FILES["image"]["type"])&&!empty($_FILES["image"]["tmp_name"])&&!empty($_FILES["image"]["type"]))
			{			
				print_r($_FILES);
			 	$img = $db->escape_value(file_get_contents($_FILES["image"]["tmp_name"]));
			 	$img_typ =$db->escape_value($_FILES["image"]["type"]);		 		 	
			 	if(substr($img_typ,0,5)==="image")
				{
					$id=$session->get_current_id();
					$query_str="UPDATE account SET image='{$img}' WHERE AID = '{$id}'";		 
					$db->query($query_str);
				}			
			}
			else 
			{
				?>
				<script type="text/javascript">
					alert("the file not allowed!");
				</script>
				<?php
			}
		}
	}
}

$message=$session->get_message();

?>

<!DOCTYPE HTML>
<!--
	Prologue by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Profile</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		
		<script src="js_profile/jquery.min.js"></script>
		<script src="js_profile/jquery.scrolly.min.js"></script>
		<script src="js_profile/jquery.scrollzer.min.js"></script>
		<script src="js_profile/skel.min.js"></script>
		<script src="js_profile/skel-layers.min.js"></script>
		<script src="js_profile/init.js"></script>
		
		
		
		<link rel="stylesheet" type="text/css" href="cssProfile/skel.css"  />
		<link rel="stylesheet" type="text/css" href="cssProfile/style.css"  />
		
		<link rel="stylesheet" type="text/css" href="cssProfile/RSStyle.css"  />
		<link rel="stylesheet" type="text/css" href="cssProfile/footer-style.css"  />
		<link rel="stylesheet" type="text/css" href="css/main_style.css">	
		
		<style>
			div.error
			{		
				color: 		 black; 
				font-weight: bold;
				font-size:   3em;
				text-align:  center;
				margin: 	 2em auto; 
				padding: 	 1em ;
				width:		 940px;
			}
			div.error ul
			{
				margin: 	 0; 
				padding-top: 1em;
			}
		</style>
	</head>
	<body>

		<!-- Header -->
		
		<div id="top_header">
				<div class="top_clearfix">
			<div class="top_logo">
				<a href="index.php"> HEAD HUNTERS </a>
			</div>
			<?php if (!($session->is_logged_in())) {?>
			<div class="login" style="font-size: 10%;">
				<a href="sign_up.php" style="text-decoration:none; color:#F5FFFF;">Sign Up </a>/ <a href="sign_in.php" style="text-decoration:none; color:#F5FFFF">Sign in</a>
			</div>
			<?php } else {?>
			<div class="login" style = "color:#F5FFFF background-color:#000F1F">
				welcome :) <a href="actions/log_out.php" style="text-decoration:underline; color:#F5FFFF">log out</a>
			</div>			
			<?php }?>
			<ul class="top_navigation">
				<li>
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="about.php">About</a>
				</li>
				<li>
					<a href="request_job.php">Request Job</a>
				</li>
				<li>
					<a href="hire.php">Hire</a>
				</li>
				<li>
					<a href="request_service.php">Request Service</a>
				</li>
				<li>
					<a href="OfferService.php">Market Service</a>
				</li>				
				<li  class="top_active">
					<a href="profile.php">Profile</a>					
				</li>
				<li>
					<a href="faq.php">FAQ</a>
				</li>
				<li>
					<a href="contact_us.php">Contact Us</a>
				</li>
			</ul>
		</div>
	</div><br></br>
	
			
		<!-- Main -->
<div id="main">

<?php echo $message; if (!empty($message)&&$message!=""&&isset($message)) {?>
	<div class="error">
		<?php echo $message?>
	</div>
<?php }?>
<section id="top"  class="one dark cover">
	<div class="container">
		<div class="top" align="left">
			<div id="logo">
				<div class="image left" >
				<font size="4"> 
				
				<?php if (!$cond) {?>
				<img src="pic.php?id=<?php echo $_SESSION['id']; ?>" alt="Mountain View" style="width:200px;height:228px">  
				
				<small>Upload your profile picture</small>
				<FORM ENCTYPE="multipart/form-data" ACTION="profile.php" METHOD="POST">
					<INPUT NAME="image" TYPE="file"  style="width:500px;height:50px" ></INPUT> 
					</br></br>
					<INPUT TYPE="submit" VALUE="upload" name="submit"  ></INPUT>
				</FORM>				
				<br/>
				<?php } else { ?>
				<img src="pic.php?id=<?php echo $visited_id ?>" alt="Mountain View" style="width:200px;height:228px">  
				<?php }?>
				</font>
				</div>

			</div>
		</div>
	</div>
	
	

<header><h2>About Me</h2></header>
<?php 
	if(!$cond)		
	{
		$account=account::find_record_by_id($session->get_current_id());
		$cv=cvs::get_cv_by_account_id($session->get_current_id());	
	}
	else
	{	
		$account=account::find_record_by_id($visited_id);
		 $cv=cvs::get_cv_by_account_id($visited_id);
	}
    echo "Username : ".$account->username."<br/>";
    echo "Email : ". $account->email."<br/>";
    echo "Lives in : ".$account->city."<br/>"."<br/>";    

    if(is_object($cv)&&!$cond)
    {    
    	if($cv->marketCV==0)	    
    		echo "to market your CV and get a job visit our REQUEST JOB link.<br>";
    	echo "<a style=\"color:black\" href=\"edit_cv.php\">edit your cv</a><br>";
    	echo "<a style=\"color:black\" href=\"actions/delete_cv.php?id={$cv->CID}\">delete your cv</a><br>";
	}
	else if(!$cond)
		echo "you do not have a CV. <a href=\"create_cv.php\" style=\"color:black;\">create your CV now</a>";
	else if(is_object($cv)&&$cond)	
		echo "<a style=\"color:black\" href=\"view_cv.php?id={$cv->CID}\">view cv</a>";	
	else if($cond)	
		echo "this profile did not create a CV yet";
	


    
?>						
					 
						 
						
</div>
</section>
	
		

	
</div>

<!-- Footer -->
		
<div id="footer">
		<div class="clearfix">
			<div class="section">
				<h4>Latest News</h4>
				<p>
					any brand new news about our website will be posted here. if any upcoming feature is added we shall announce here.
				</p>
			</div>
			<div class="section contact">
				<h4>Contact Us</h4>				
				<p>
					<span>Phone:</span> (+20) 012 0881 6682
				</p>
				<p>
					<span>Email:</span> HeadHunters@AUC-CU.com
				</p>
			</div>
			<div class="section">
				<h4>SEND US A MESSAGE</h4>
				<p>
					If you're having problems through any part of this website. Contact Us or visit our forum pages.
				</p>
				<a href="#" class="subscribe">Click to send us an email</a>
			</div>
		</div>
		
		<div id="footnote">
			<div class="clearfix">
				<div class="connect">
					<a href="#" class="facebook"></a><a href="#" class="twitter"></a><a href="#" class="googleplus"></a><a href="#" class="pinterest"></a>
				</div>
				<p>
					© Copyright 2015 @AUC-CU. All Rights Reserved.
				</p>
			</div>
		</div>
	</div>
			

	</body>
</html>