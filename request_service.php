<?php
include("includes/initialize.php");
if(!$session->is_logged_in())
	redirect_to("sign_in.php");

$tags=tags::retrieve_all_tags();		
if (isset($_POST['submit'])) 
{													
	$service=new RequestService();
	$service->rdescription=$_POST['Descr'];
	$service->price=$_POST['Price'];
	$service->duration=$_POST['Dead_Line'];
	$service->AID=$_SESSION['id'];
	$service->tags=$_POST['Tags'];	
	if (!($service->create() && $service->insert_tags()))		
		die("failed to insert service and its tags");

}								

?>

<!DOCTYPE html>
<html>
<head>
<title>Head Hunters/Request Service</title>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href="css/sign_up_style.css" media="all" />
	<link rel="stylesheet" href="css/request_service_style.css" type="text/css" media="all"/>
	<link rel="stylesheet" href="css/main_style.css" type="text/css" media="all"/>

	<script type="text/javascript" src="js/js_functions.js"></script>
</head>
<body>
	<div class="container" style = "height: 1000px;">
			<div id="header">
			<div class="clearfix">
				<div class="logo">
					<a href="index.php" style="text-decoration:none; color:#b8c6ac; font-size:3em;">HEAD HUNTERS</a>
				</div>
				<?php  if (!$session->is_logged_in()) {?>
				<div class="login">
					<a href="sign_up.php" style="text-decoration:none; color:#b8c6ac">Sign Up </a>/ <a href="sign_in.php" style="text-decoration:none; color:#b8c6ac">Sign in</a>
				</div>
				<?php } else {?>
				<div class="login">
					welcome :) <a href="actions/log_out.php" style="text-decoration:underline; color:#b8c6ac">log out</a>
				</div>			
				<?php }?>
				<ul class="navigation">
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="about.php">About</a>
					</li>
					<li>
						<a href="request_job.php">Request Job</a>
					</li>
					<li>
						<a href="hire.php">Hire</a>
					</li>
					<li class="active">
						<a href="request_service.php">Request Service</a>
					</li>
					<li>
						<a href="OfferService.php">Market Service</a>
					</li>				
					<li>
						<a href="profile.php">Profile</a>					
					</li>
					<li>
						<a href="faq.php">FAQ</a>
					</li>
					<li>
						<a href="contact_us.php">Contact Us</a>
					</li>
				</ul>
			</div>
			</div><br></br>
			<header>
			<h1 style = "color: #0000;">Request Service</h1>
			<span style = "color: #0000;">You can either search for someone to offer you the service or fill in a form with the required service</span>
            </header>     

			<!----------------------Search Box-------------------------->

			<div class = "Search">
			<form action = "search.php">
			<input type = "text" name = "search" placeholder = "The required service is..."/>
			<button type="submit">Search</button>
			</form>
			</div><br></br>
			
			<!----------------------------------------Request Service form------------------------------------>
			<div  class="form" style = "height:450px;">
				<form id="contactform" method="POST" action = "request_service.php"> 
					<p class="contact"><label for="Description">Description:</label></p> 
					<input id="Desc" name="Descr" placeholder="Desription of the requested service." required="" tabindex="1" pattern="{10,300}" type="text"> 
					
					<p class="contact"><label for="Price">Price</label></p> 
					<input id="Rprice" name="Price" placeholder="The expected price of the service in dollars" required="" tabindex="1" type="number" style="width: 400px"> 
					
					<p class="contact"><label for="Dead_Line">Dead Line:</label></p> 
					<input id="RDeadLine" name="Dead_Line" placeholder="The duration of the service in hours" required="" tabindex="1" type="number" style="width: 400px"> 
        
		<!-------------------------------Tags------------------------------------------>

					  <div id="greater_selection">
					  <p class = "contact"><label>Search Tags</label></p>					 
					  	<div class="small_selection" id="original_selection">
						  <select class="select-style Tags" name="Tags[]" style = "width: 400px" id="selections">
						  	<?php					  		
						  		foreach ($tags as $a_tag)
						  		{
						  			$output = "<option value=\"".$a_tag->TID."\">".$a_tag->tname."</option>";
						  			echo $output;
						  		}
						  	?>
						  </select><br/><br/>
						 </div>
					  </div>
					  <button name="addtags" type="button" onclick="create_selection_tag()">add another tag</button>
					  <br/><br/>
				
				<input class="buttom" name="submit" id="submit" tabindex="5" value="Submit" type="submit"/> 
				</form> 
			</div><br></br>
			

			<div id="footer">
			<div class="clearfix">
				<div class="section">
					<h4>Latest News</h4>
					<p>
						any brand new news about our website will be posted here. if any upcoming feature is added we shall announce here.
					</p>
				</div>
				<div class="section contact">
					<h4>Contact Us</h4>				
					<p>
						<span>Phone:</span> (+20) 012 0881 6682
					</p>
					<p>
						<span>Email:</span> HeadHunters@AUC-CU.com
					</p>
				</div>
				<div class="section">
					<h4>SEND US A MESSAGE</h4>
					<p>
						If you're having problems through any part of this website. Contact Us or visit our forum pages.
					</p>
					<a href="#" class="subscribe">Click to send us an email</a>
				</div>
			</div>
			<div id="footnote">
				<div class="clearfix">
					<div class="connect">
						<a href="#" class="facebook"></a><a href="#" class="twitter"></a><a href="#" class="googleplus"></a><a href="#" class="pinterest"></a>
					</div>
					<p>
						© Copyright 2015 @AUC-CU. All Rights Reserved.
					</p>
				</div>
			</div>
		</div>
	   </form> 
	</div>      
</div>
</body>
</html>
