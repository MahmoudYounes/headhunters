<?php
require_once ("includes/initialize.php");
if(!$session->is_logged_in())
	redirect_to("index.php");
$id=$session->get_current_id();
$account=account::find_record_by_id($id);
$cv=cvs::get_cv_by_account_id($id);
$sections=cv_section::get_sections_of_cv($cv->CID);

if (isset($_POST['submit']))
{			
	$section_names=$_POST['section-name'];
	$section_contents=$_POST['section-content'];	
	for ($i=0;$i<count($section_names);$i++)
	{
		$sections[$i]->section_name=$section_names[$i];
		$sections[$i]->section_content=$section_contents[$i];
		$sections[$i]->update();
	}
	$session->set_message("CV updated Successfuly");	
}
$message=$session->get_message();
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Head Hunters</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
	<script type="text/javascript" src="js/js_functions.js"></script>
	<style>
	.cv-section
	{
		margin-top: 3em;
		width: 50%;
		margin: auto;				
	}	
	div.error
	{		
		color: 		 black; 
		font-weight: bold;
		font-size:   3em;
		text-align:  center;
		margin: 	 2em auto; 
		padding: 	 1em ;
		width:		 940px;
	}
	div.error ul
	{
		margin: 	 0; 
		padding-top: 1em;
	}

	</style>
</head>
<body>
<div style = "background-color:#000F1F;">
	<div id="header" style = "background-color:#000F1F;">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="text-decoration:none; color:#F5FFFF; font-size:3em;">HEAD HUNTERS</a>
			</div>
			<?php if (!($session->is_logged_in())) {?>
			<div class="login">
				<a href="sign_up.php" style="text-decoration:none; color:#F5FFFF;">Sign Up </a>/ <a href="sign_in.php" style="text-decoration:none; color:#F5FFFF">Sign in</a>
			</div>
			<?php } else {?>
			<div class="login" style = "color:#F5FFFF background-color:#000F1F">
				welcome :) <a href="actions/log_out.php" style="text-decoration:underline; color:#F5FFFF">log out</a>
			</div>			
			<?php }?>
	
		
			<ul class="navigation">
				<li>
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="about.php">About</a>
				</li>
				<li>
					<a href="request_job.php">Request Job</a>
				</li>
				<li>
					<a href="hire.php">Hire</a>
				</li>
				<li>
					<a href="request_service.php">Request Service</a>
				</li>
				<li>
					<a href="OfferService.php">Market Service</a>
				</li>				
				<li class="active">
					<a href="profile.php">Profile</a>					
				</li>
				<li>
					<a href="faq.php">FAQ</a>
				</li>
				<li>
					<a href="contact_us.php">Contact Us</a>
				</li>
			</ul>
		</div>
	</div>
	</div>
	<div id="contents">	
	<?php if (!empty($message)&&$message!=""&&isset($message)) {?>
			<div class="error">
				<?php echo $message?>
			</div>
	<?php }?>	
		<div class="highlight">
			<div class="clearfix">
				<a href="profile.php">back to profile</a>
				<div class="cv-section">
				<form method="POST" action="edit_cv.php">				
					<table id="section-table">
						<?php
						foreach ($sections as $section)
						{
							$name=trim($section->section_name);
							$content=trim($section->section_content);
							echo "<tr>";
							echo "<td>section name</td>";
							echo "</tr>";
							echo "<tr>";
							echo "<td><input style=\"width:25em\" type=\"text\" name=\"section-name[]\" value=\"{$name}\"></td>";
							echo "</tr>";
							echo "<tr>";
							echo "<td>section content</td>";
							echo "</tr>";
							echo "<tr>";
							echo "<td><textarea style=\"width:30em;height:30em\"  rows=\"20\" cols=\"300\" name=\"section-content[]\">{$content}</textarea></td>";
							echo "</tr>";
							echo "<tr>";
							echo "<td><hr></td>";
							echo "</tr>";
						}
						?>
						<tr>
							<td><input type="submit" name="submit"></td>
						</tr>
					</table>
					</form>					
				</div>

			</div>
		</div>
	</div>
</body>
</html>

