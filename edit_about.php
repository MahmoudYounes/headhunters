<?php
require_once("includes/initialize.php");

if(!$session->is_admin())
	redirect_to("index.php");
$admin_id=$session->get_current_id();
$contentObj=PagesContent::get_content_by_name("about_content");
if (isset($_POST['update']))
{
	//========================================\\
	//TODO : Verify Data and apply secuirty Standards

	//========================================\\
	$string=$_POST['content'];
	$contentObj->content=$string;
	$contentObj->update();	
	$session->set_message("update is successful");
}
if(is_object($contentObj))	
	$contentString=$contentObj->content;	
else 
	$contentString="";

$message=$session->get_message();
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Head Hunters</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
	<style type="text/css">
	div.error
	{		
		color: 		 black; 
		font-weight: bold;
		font-size:   3em;
		text-align:  center;
		margin: 	 2em auto; 
		padding: 	 1em ;
		width:		 940px;
	}
	div.error ul
	{
		margin: 	 0; 
		padding-top: 1em;
	}
	textarea
	{
		width: 50em;
		height: 30em;
	}

	</style>
</head>

<body>
<div id="header" style = "background-color:#000F1F;">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="padding-left:6.2em;text-decoration:none; color:#F5FFFF; font-size:3em;">HEAD HUNTERS</a>
			</div>
		</div>
</div>
<div id="contents">
	<div class="highlight">
		<div class="clearfix">
			<?php if (!empty($message)&&$message!=""&&isset($message)) {?>
			<div class="error">
				<?php echo $message?>
			</div>
			<?php }?>
			<a href="AdminIndex.php?id=<?php echo $admin_id?>" style="float:right;">Admin Index</a>
			<form method="POST" action="edit_about.php">
				<table>
					<tr>
						<td>about page content</td>
					</tr>
					<tr>
						<td>
							<textarea row="30" col="200" name="content" required><?php  echo $contentString?></textarea>
						</td>										
					</tr>									
					<tr>
						<td><input type="submit" name="update" value="update"></td>
					</tr>
				</table>
			</form>					
	  	</div>
	</div>
</div>	