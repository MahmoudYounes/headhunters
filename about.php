<?php 
include("includes/initialize.php");

$about_content=PagesContent::get_content_by_name("about_content")->content;
$news=PagesContent::get_content_by_name("home_news")->content;

?>
<!DOCTYPE HTML>
<!-- Website template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>About - Head Hunter</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
</head>
<body>
	<div id="header">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="text-decoration:none; color:#b8c6ac; font-size:3em;">HEAD HUNTERS</a>
			</div>
			<?php  if (!$session->is_logged_in()) {?>
			<div class="login">
				<a href="sign_up.php" style="text-decoration:none; color:#b8c6ac">Sign Up </a>/ <a href="sign_in.php" style="text-decoration:none; color:#b8c6ac">Sign in</a>
			</div>
			<?php } else {?>
			<div class="login">
				welcome :) <a href="actions/log_out.php" style="text-decoration:underline; color:#b8c6ac">log out</a>
			</div>			
			<?php }?>
			<ul class="navigation">
				<li>
					<a href="index.php">Home</a>
				</li>
				<li class="active">
					<a href="about.php">About</a>
				</li>
				<li>
					<a href="request_job.php">Request Job</a>
				</li>
				<li>
					<a href="hire.php">Hire</a>
				</li>
				<li>
					<a href="request_service.php">Request Service</a>
				</li>
				<li>
					<a href="OfferService.php">Market Service</a>
				</li>				
				<li>
					<a href="profile.php">Profile</a>					
				</li>
				<li>
					<a href="faq.php">FAQ</a>
				</li>
				<li>
					<a href="contact_us.php">Contact Us</a>
				</li>
			</ul>
		</div>
	</div>
	<div id="contents">
	<div class="featured">
			<h2>Why Choose Us?</h2>
			<ul class="clearfix">
				<li>
					<div class="frame1">
						<div class="box">
							<img src="images/meeting.jpg" alt="Img" height="130" width="197">
						</div>
					</div>
					<p>
						<b>Our Applications</b> Our website contains the most skillful applicants with their CVs revised by us to assure quality of the job.
					</p>
					<a href="about.php" class="more">Read More</a>
				</li>
				<li>
					<div class="frame1">
						<div class="box">
							<img src="images/handshake.jpg" alt="Img" height="130" width="197">
						</div>
					</div>
					<p>
						<b>Support</b> revise our FAQ page or post your questions on forums to get helpful support from our users and admins. check out about us.
					</p>
					<a href="index.php" class="more">Read More</a>
				</li>
				<li>
					<div class="frame1">
						<div class="box">
							<img src="images/smile.jpg" alt="Img" height="130" width="197">
						</div>
					</div>
					<p>
						<b>what we offer</b> for Head Hunters: get your job done quickly with quality. for applicants: start your career right now and get paid.
					</p>
					<a href="index.php" class="more">Read More</a>
				</li>
				<li>
					<div class="frame1">
						<div class="box">
							<img src="images/family-small.jpg" alt="Img" height="130" width="197">
						</div>
					</div>
					<p>
						<b>get in touch with us</b> we would love to hear from you. visit our Contact Us page to submit a message or know all our contact info.
					</p>
					<a href="index.php" class="more">Read More</a>
				</li>
			</ul>
		</div>
		<div class="clearfix">
			<h1>About Us</h1>
			<p>
				<?php echo $about_content ?>
			</p>	
		</div>
	</div>
	<div id="footer">
		<div class="clearfix">
			<div class="section">
					<h4>Latest News</h4>
				<p>
					<?php
						echo $news;
					?>
				</p>
			</div>
			<div class="section contact">
				<h4>Contact Us</h4>				
				<p>
					<span>Phone:</span> (+20) 012 0881 6682
				</p>
				<p>
					<span>Email:</span> HeadHunters@AUC-CU.com
				</p>
			</div>
			<div class="section">
				<h4>SEND US A MESSAGE</h4>
				<p>
					If you're having problems through any part of this website. Contact Us or visit our forum pages.
				</p>
				<a href="#" class="subscribe">Click to send us an email</a>
			</div>
		</div>
		<div id="footnote">
			<div class="clearfix">
				<div class="connect">
					<a href="#" class="facebook"></a><a href="#" class="twitter"></a><a href="#" class="googleplus"></a><a href="#" class="pinterest"></a>
				</div>
				<p>
					© Copyright 2015 @AUC-CU. All Rights Reserved.
				</p>
			</div>
		</div>
	</div>
</body>
</html>