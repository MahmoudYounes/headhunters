<?php
require_once("table_class.php");
require_once("database_class.php");

class faq extends table
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="faq";
	protected static $db_fields=['QAID','question','answer'];
	//=================================================\\

	public $id="QAID";
	public $QAID;
	public $question;
	public $answer;

	public static function get_all_questions()
	{
		return self::find_all_records();
	}

	public function format_question_for_output_in_table()
	{
		$output=
		"
		<td>{$this->question}</td>
		<td>{$this->answer}</td>
		<td><a href=\"actions/remove_question.php?id={$this->QAID}\">remove</a> / <a href=\"actions/update_question.php?id={$this->QAID}\"> update</a></td>
		";
		return $output;
	}
}

?>