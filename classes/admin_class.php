<?php
require_once("database_class.php");
require_once("table_class.php");


class admin extends table
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="admins";
	protected static $db_fields=['ADID','username','password','email','privilege_level'];
	//=================================================\\

	public $id="ADID";
	public $ADID;
	public $username;
	public $email;
	public $password;
	public $privilege_level;

	public static function authenticate($username="",$password="")
	{
		global $db;

		$safe_username=$db->escape_value($username);
		$safe_password=$db->escape_value($password);			

		$query  = "SELECT * ";
		$query .= "FROM ".self::$table_name." ";
		$query .= "WHERE username='{$safe_username}' ";		
		$query .= "LIMIT 1";		
		
		$admin=self::find_by_sql($query);		
		
		if($admin && password_check($password,$admin->password)) //must use function password check later					
			return $admin;
		
		return false;
	}
	public static function build_on_clearence_level($level)
	{
		if(!isset($level))
		{
			$output= "something is wrong";
			return $output;
		}
		switch ($level) {
			case 0:
				{
					$output=
					"
					<ul>
						<li>
							<a href=\"edit_tags.php\">edit tags</a>
						</li>
						<li>
							<a href=\"edit_home_page.php\">edit home page context</a>
						</li>
						<li>
							<a href=\"edit_home_quote.php\">edit home page quote</a>
						</li>
						<li>
							<a href=\"edit_home_news.php\">edit home page recent news</a>
						</li>
						<li>
							<a href=\"edit_about.php\">edit about page</a>
						</li>
						<li>
							<a href=\"edit_faq.php\">edit FAQ page</a>
						</li>
						<li>
							<a href=\"manage_admins.php\">manage admins</a>
						</li>
						<li>
							<a href=\"edit_cvs.php\">view and revice CVs</a>
						</li>						
					</ul>
					";
					break;
				}
			case 1:
				{
					$output=
					"
					<ul>
						<li>
							<a href=\"edit_tags.php\">edit tags</a>
						</li>				
					</ul>
					";
					break;
				}	
			case 2:
				{
					$output=
					"
					<ul>
						<li>
							<a href=\"edit_home_page.php\">edit home page context</a>
						</li>
						<li>
							<a href=\"edit_home_quote.php\">edit home page quote</a>
						</li>
						<li>
							<a href=\"edit_home_news.php\">edit home page recent news</a>
						</li>	
					</ul>
					";
						break;
				}
			case 3:
				{
					$output=
					"
					<ul>
						<li>
							<a href=\"edit_about.php\">edit about page</a>
						</li>
						<li>
							<a href=\"edit_faq.php\">edit FAQ page</a>
						</li>
					</ul>
					";
					break;
				}
			default:
				$output="you are authorized to do nothing :P .";
				break;
		}
		return $output;
	}

	public function format_admin_for_output_in_table()
	{
		$output=
		"
		<td>{$this->username}</td>
		<td>{$this->email}</td>
		<td>{$this->privilege_level}</td>
		<td><a href=\"actions/remove_admin.php?id={$this->ADID}\">remove</a>
		";
		return $output;

	}

	public static function check_if_admin($id)
	{				
		if(self::find_record_by_id($id))
			return true;
		return false;
	}

	public static function retrieve_all_admins()
	{	
		return self::find_all_records();
	}
	protected function set_id($id)
	{
		$this->ADID=$id;
	}
}