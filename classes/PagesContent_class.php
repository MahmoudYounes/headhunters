<?php
require_once("database_class.php");
require_once("table_class.php");

class PagesContent extends Table
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="pages_content";
	protected static $db_fields=['PCID','content_name','content'];
	//=================================================\\

	public $id="PCID";
	public $PCID;
	public $content_name;
	public $content;

	public static function get_content_by_name($name)
	{
		global $db;
		$safe_name=$db->escape_value($name);		

		$query  = "SELECT * ";
		$query .= "FROM ".self::$table_name." ";
		$query .= "WHERE content_name='".$safe_name."' ";
			
		return self::find_by_sql($query);	
	}
	
}



?>