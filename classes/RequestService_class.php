<?php
require_once("table_class.php");
require_once("database_class.php");

class RequestService extends Table
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="service_requests";
	protected static $db_fields=['duration','price','rdescription','AID'];
	//=================================================\\

	/* public vars */
	public $id="RID";
	public $RID;
	public $duration;
	public $price;
	public $rdescription;
	public $AID;	
	public $tags;			//array of tags of service
	//=================================================\\

	private function get_clean_tags()
	{
		global $db;
		$clean_tags=array();
		foreach ($this->tags as $tag)
		{
			$clean_tags[]=$db->escape_value($tag);
		}
		return $clean_tags;
	}

	protected function set_id($id)
	{
		$this->RID=$id;
	}

	public function insert_tags()
	{
		global $db;

		$in_RID=$this->RID;
		$clean_tags=$this->get_clean_tags();				
		foreach ($clean_tags as $tag)
		{					
			$query  = "INSERT INTO request_tags (";
			$query .= "RID,TID";
			$query .= ") VALUES (";
			$query .= "{$in_RID},{$tag})";
			
			if(!($db->query($query)))
				return false;
		}
		return true;	
	}

}





?>