<?php
require_once("database_class.php");
require_once("table_class.php");


class Account extends Table
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="account";
	protected static $db_fields=['AID','first_name','middle_name','last_name','email','username','password','b_date','country','sex','account_type',
	'type_payment','city','st_name','st_no'];
	//=================================================\\

	/* public vars */

	public $id="AID";
	public $AID;
	public $first_name;
	public $middle_name;
	public $last_name;
	public $email;
	public $username;
	public $password;
	public $b_date;
	public $country;
	public $sex;	
	public $account_type;
	public $type_payment;
	public $city;
	public $st_name;
	public $st_no;	

	//=================================================\\

	public static function authenticate($username="",$password="")
	{
		global $db;

		$safe_username=$db->escape_value($username);
		$safe_password=$db->escape_value($password);			

		$query  = "SELECT * ";
		$query .= "FROM ".self::$table_name." ";
		$query .= "WHERE username='{$safe_username}' ";		
		$query .= "LIMIT 1";		

		$account=self::find_by_sql($query);		
		
		if($account && password_check($password,$account->password)) //must use function password check later					
			return $account;
		
		return false;
	}	

	public static function retrieve_all_accounts()
	{				
		return self::find_all_records();
	}

	public static function find_account_by_username($username)
	{			
		return self::find_record_by_id($username);	
	}

	public static function find_account_by_id($id)
	{		
		return self::find_record_by_id($id);	
	}
	

	protected function set_id($id)
	{
		$this->AID=$id;
	}
}

?>