<?php
require_once("database_class.php");
require_once("table_class.php");


class cv_section extends Table
{
	protected static $table_name="cv_section";
	protected static $db_fields=['SCID','CID','section_name','section_content'];
	//=================================================\\

	/* public vars */

	public $id="SCID";
	public $SCID;
	public $CID;
	public $section_name;
	public $section_content;

	public static function get_sections_of_cv($id)
	{
		global $db;

		$safe_id=$db->escape_value($id);

		$query  = "SELECT * ";
		$query .= "FROM cv_section ";
		$query .= "WHERE CID={$safe_id}";

		return self::find_by_sql($query);
	}

	public function format_section_for_output_in_table()
	{

		$output=
		"
		<td>{$this->tname}</td>
		<td>{$this->tdescription}</td>
		<td><a href=\"actions/remove_tag.php?id={$this->TID}\">remove</a> / <a href=\"actions/update_tag.php?id={$this->TID}\">update</a></td>
		";
		return $output;
	}
}
