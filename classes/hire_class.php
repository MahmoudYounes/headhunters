<?php
require_once("includes/initialize.php");

class Hire extends Table 
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="hire";
	protected static $db_fields=['AID','Jdescription','Salary'];
	//=================================================\\

	/* public vars */
	public $id="JID";
	public $JID;	
	public $Jdescription;
	public $AID;	
	public $tags;			//array of tags of service
	public $Salary;
	//=================================================\\


	private function get_clean_tags()
	{
		global $db;
		$clean_tags=array();
		foreach ($this->tags as $tag)
		{
			$clean_tags[]=$db->escape_value($tag);
		}
		return $clean_tags;
	}

	protected function set_id($id)
	{		
		$this->JID=$id;
	}

	public function insert_tags()
	{
		global $db;

		$in_JID=$this->JID;
		$clean_tags=$this->get_clean_tags();				
		foreach ($clean_tags as $tag)
		{					
			$query  = "INSERT INTO job_tags (";
			$query .= "JID,TID";
			$query .= ") VALUES (";
			$query .= "{$in_JID},{$tag})";
			
			if(!($db->query($query)))
				return false;
		}
		return true;	
	}
}

?>