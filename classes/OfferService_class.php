<?php
require_once("includes/initialize.php");

class OfferService extends Table 
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="service_offers";
	protected static $db_fields=['AID','sdescription'];
	//=================================================\\

	/* public vars */
	public $id="SID";
	public $SID;	
	public $sdescription;
	public $AID;	
	public $tags;			//array of tags of service
	//=================================================\\


	private function get_clean_tags()
	{
		global $db;
		$clean_tags=array();
		foreach ($this->tags as $tag)
		{
			$clean_tags[]=$db->escape_value($tag);
		}
		return $clean_tags;
	}

	protected function set_id($id)
	{		
		$this->SID=$id;
	}

	public function insert_tags()
	{
		global $db;

		$in_SID=$this->SID;
		$clean_tags=$this->get_clean_tags();				
		foreach ($clean_tags as $tag)
		{					
			$query  = "INSERT INTO service_tags (";
			$query .= "SID,TID";
			$query .= ") VALUES (";
			$query .= "{$in_SID},{$tag})";
			
			if(!($db->query($query)))
				return false;
		}
		return true;	
	}
}

?>