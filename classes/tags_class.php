<?php
require_once("table_class.php");
require_once("database_class.php");

class tags extends Table
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="tags";
	protected static $db_fields=['TID','tname','tdescription'];

	//=================================================\\

	/* public vars */
	public $id='TID';
	public $TID;
	public $tname;	
	public $tdescription;
	//=================================================\\

	public static function retrieve_all_tags()
	{
		return self::find_all_records();
	}

	public function format_tag_for_output_in_table()
	{

		$output=
		"
		<td>{$this->tname}</td>
		<td>{$this->tdescription}</td>
		<td><a href=\"actions/remove_tag.php?id={$this->TID}\">remove</a> / <a href=\"actions/update_tag.php?id={$this->TID}\">update</a></td>
		";
		return $output;
	}
	protected function set_id($id)
	{
		$this->TID=$id;
	}
	

}
