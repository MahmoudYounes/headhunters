<?php
require_once("includes/initialize.php");
if(!$session->is_admin())
	redirect_to("index.php");
$admin_id=$session->get_current_id();
$admin=admin::find_record_by_id($admin_id);

if(isset($_POST['update']))
{
	//======================================================\\
	//TODO : validate data

	//======================================================\\
	$errors="<ul>";
	$username=$_POST['adusername'];	
	$password=$_POST['adpassword'];
	$re_password=$_POST['re-adpassword'];
	$email=$_POST['ademail'];
	if(!empty($username)&&$username!="")
		$admin->username=$username;
	else
		$errors.="<li>username can not be empty</li>";
	if (!empty($password)&&$password!="")
	{
		if($password==$re_password)
			$admin->password=password_encrypt($password);
		else
			$errors.="<li>passwords does not match</li>";
	}
	if(!empty($email)&&$email!="")
		$admin->email=$email;
	else
		$errors.="<li>email can not be empty</li>";
	if ($errors == "<ul>")	
	{
		$admin->update();
		$errors="updated successfuly";
	}
	else
		$errors.="</ul>";
	$session->set_message($errors);
}
$message=$session->get_message();

?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Head Hunters</title>
	<link rel="stylesheet" href="css/main_style.css" type="text/css">
	<style type="text/css">
	div.error
	{		
		color: 		 black; 
		font-weight: bold;
		font-size:   3em;
		text-align:  center;
		margin: 	 2em auto; 
		padding: 	 1em ;
		width:		 940px;
	}
	div.error ul
	{
		margin: 	 0; 
		padding-top: 1em;
	}
	.table
	{		
		width:  100%;	
		text-align: center;
	}
	

	</style>
</head>

<body>
<div id="header" style = "background-color:#000F1F;">
		<div class="clearfix">
			<div class="logo">
				<a href="index.php" style="padding-left:6.2em;text-decoration:none; color:#F5FFFF; font-size:3em;">HEAD HUNTERS</a>
			</div>
		</div>
</div>
<div id="contents">
	<div class="highlight">
		<div class="clearfix">
			<?php if (!empty($message)&&$message!=""&&isset($message)) {?>
			<div class="error">
				<?php echo $message?>
			</div>
			<?php }?>	
			<p><a href="AdminIndex.php?id=<?php echo $admin_id?>">admin index</a></p>	
			<form method="POST" action="edit_admin_info.php">
				<table>
					<tr>
						<td>username</td>
					</tr>
					<tr>
						<td><input type="text" name="adusername" value="<?php echo $admin->username ?>"></td>
					</tr>
					<tr>
						<td>enter new password</td>
					</tr>
					<tr>
						<td><input type="password" name="adpassword"></td>
					</tr>
					<tr>
						<td>re enter new password</td>
					</tr>
					<tr>
						<td><input type="password" name="re-adpassword"></td>
					</tr>
					<tr>
						<td>email</td>
					</tr>
					<tr>
						<td><input type="email" name="ademail" value="<?php echo $admin->email ?>">
					</tr>
					<tr>
						<td><input type="submit" name="update"></td>
					</tr>
				</table>
			</form>


		</div>
	</div>
</div>