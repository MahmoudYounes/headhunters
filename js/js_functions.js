function check_validity_of_signup_form()
{
	//=================================================================================\\
	var errors="please fix the following errors:\n";
	var error_cond=0;
	var xmlhttp;	
	if (window.XMLHttpRequest)
  	{// code for IE7+, Firefox, Chrome, Opera, Safari
  		xmlhttp=new XMLHttpRequest();
  	}
	else
  	{// code for IE6, IE5
  		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
  	var doc=document.getElementById("username");
  	var get_request="actions/check_username.php?username="+doc.value;  	
  	xmlhttp.open("GET",get_request,false);
	xmlhttp.send();
	var result = xmlhttp.responseText;	
	if (result=="invalid")	
	{
		errors+="-username already taken\n";				
		error_cond=1;
	}
	
	//=================================================================================\\ checks on password and repassword

	var password=document.getElementById("password");
	var repassword=document.getElementById("repassword");

	if(password.value!=repassword.value)
	{
		errors+="-passwords does not match\n";
		error_cond=1;
	}
	//=================================================================================\\

	if (error_cond>0)
	{
		alert(errors);
		return false;
	}
	return true;
}

var counter=1;
function create_selection_tag()
{			
	if (counter==5)
	{
		alert("can't add more tags");
		return;
	}

	var main_div=document.getElementById("greater_selection");

	var original_selection=document.getElementById("selections");		// selection tag
	
	var new_elem=document.createElement('Select');
	new_elem.name="Tags[]";
	new_elem.className="select-style Tags";
	new_elem.style.width="400px";

	for (var i=0;i<original_selection.length;i++)
	{
		var new_option=document.createElement('option');				
		new_option.value=original_selection[i].value;
		new_option.text=original_selection[i].text;			
		new_elem.appendChild(new_option)		
	} 	
	var br1 = document.createElement("br");
	var br2 = document.createElement("br");
		
	main_div.appendChild(new_elem);
	main_div.appendChild(br1);
	main_div.appendChild(br2);
	console.log(main_div);
	counter++;
}

var section_counter=1;
var position=4;
function add_cv_section()
{
	if(position/5>=4) //max 5 sections
		return;

	var table=document.getElementById("section-table");
	
	//creating hr tag
	var row5=table.insertRow(position++);
	var cell51=row5.insertCell(0);
	var hr=document.createElement("hr");
	cell51.appendChild(hr);

	//creating section name
	var row1=table.insertRow(position++);
	var cell11=row1.insertCell(0);
	cell11.innerHTML="section name";

	var row2=table.insertRow(position++);
	var cell21=row2.insertCell(0);
	var field1=document.createElement("input");
	field1.type="text";
	field1.name="section_name[]";
	field1.style.width="25em";
	cell21.appendChild(field1);

	//creating section content
	var row3=table.insertRow(position++);
	var cell31=row3.insertCell(0);
	cell31.innerHTML="section content";

	var row4=table.insertRow(position++);
	var cell41=row4.insertCell(0);
	var field2=document.createElement("textarea");
	field2.rows="20";
	field2.cols="300";
	field2.style.width="30em";
	field2.style.height="30em";
	field2.name="section_content[]";
	cell41.appendChild(field2);
}